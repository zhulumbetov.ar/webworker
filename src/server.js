const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

  app.post('/subscribe', (req, res) => {
    const email = req.body.email;
    // do something with the email
    setTimeout(() => {
      res.status(200).send(`Subscription successful! Email: ${email}`);
    }, 123);
  });

  app.post('/forbidden', (req, res) => {
      res.status(422).send(`forbidden is not allowed`);
  });

  app.post('/unsubscribe', (req, res) => {
    const email = req.body.email;
  
    // do something with the email
    setTimeout(() => {
      res.status(200).send(`Unsubscription successful! Email: ${email}`);
    }, 123);
    
  });
  app.post('/analytics/user', (req, res) => {
    const { events } = req.body;
    console.log(events);
    res.status(200).send('Analytics data received');
  });
  

  app.get('/community', (req, res) => {
    const fs = require('fs');
    const data = JSON.parse(fs.readFileSync('./1.json'));
  
    res.send(data);
  });
  

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
