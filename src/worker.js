let events = [];

self.addEventListener('message', function(event) {
  events.push(event.data);

  if (events.length === 5) {
    const xhr = new XMLHttpRequest();
    const url = 'http://localhost:3000/analytics/user';
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200) {
        console.log(JSON.stringify(xhr.responseText));
      }
    };
    const data = JSON.stringify({ events: events });
    xhr.send(data);
    events = [];
  }
});
